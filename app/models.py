from django.db import models
from django.db.models import EmailField
from django.core.validators import EmailValidator, RegexValidator

from django.utils import timezone


GUESTS_NUMBER = [
    ('1', "1 person. Single comfort room"),
    ('2', "2 people. Recommended Double, Twin or King room"),
    ('3', "3 people. Double-Double or Luxury will match your needs"),
    ('4', "4 people. Double-Double or Luxury will match your needs"),
    ('5', "5 people. Our Luxury room is for you!")
]
STAT = [
    ('0', "Waits for payment"),
    ('1', "Paid")
]


class Price(models.Model):
    price = models.IntegerField(null=False)

    def __str__(self):
        return str(self.price)


class Room(models.Model):
    room_type = models.TextField(max_length=40)
    price = models.ForeignKey(Price, null=False, on_delete=models.PROTECT)
    description = models.TextField(max_length=1000, null=True, blank=True)

    def __str__(self):
        return self.room_type


class Booking(models.Model):
    check_in = models.DateField(default=timezone.now)
    check_out = models.DateField(default=timezone.now)
    room = models.ForeignKey(Room, null=True, on_delete=models.CASCADE)
    guests_number = models.CharField(max_length=200, choices=GUESTS_NUMBER, default=2)
    name = models.TextField(max_length=50)
    surname = models.TextField(max_length=100)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone = models.CharField(validators=[phone_regex], max_length=17, blank=True)
    email = EmailField(max_length=254, blank=False, validators=[EmailValidator()])
    created_at = models.DateTimeField(auto_now_add=True)
    payment_status = models.CharField(max_length=200, choices=STAT, default='0')

    def __str__(self):
        return self.surname


class Comment(models.Model):
    name = models.TextField(max_length=50)
    surname = models.TextField(max_length=100)
    rating = models.IntegerField(null=True)
    comment = models.TextField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.surname
