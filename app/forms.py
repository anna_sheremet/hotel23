from app.models import Booking, Comment, Room
import app.forms
from django.forms import ModelForm
from django_flatpickr.widgets import DatePickerInput


class BookingForm(ModelForm):

    class Meta:
        model = Booking
        fields = ["check_in", "check_out", "room", "guests_number", "name", "surname", "phone", "email", ]
        widgets = {
            "check_in": DatePickerInput(),
            "check_out": DatePickerInput(),
        }


class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ["name", "surname", "rating", "comment", ]