from django.core.mail import BadHeaderError, EmailMultiAlternatives
from django.utils.html import strip_tags
import os

from django.http import HttpResponse
from django.shortcuts import render

from datetime import timedelta

from app.forms import BookingForm, CommentForm
from app.models import Price, Comment, Room, Booking

from django.conf import settings


# Create your views here.
def home(request):
    form = BookingForm()
    return render(request, "index.html", {'form': form})


def check_availability(new_booking):
    bookings_within_period = Booking.objects.filter(
        check_in__lt=new_booking.check_out,
        check_out__gt=new_booking.check_in
    )

    bookings_of_room_type = bookings_within_period.filter(room=new_booking.room)

    occupancy_per_day = {}

    for booking in bookings_of_room_type:
        start_date = max(new_booking.check_in, booking.check_in)
        end_date = min(new_booking.check_out, booking.check_out)
        delta = (end_date - start_date).days

        for i in range(delta):
            current_date = start_date + timedelta(days=i)
            occupancy_per_day[current_date] = occupancy_per_day.get(current_date, 0) + 1

    for date, occupancy in occupancy_per_day.items():
        if occupancy > 3:
            return False

    return True


def calculate_price(new_booking):
    night_price, price, diff = 0, 0, 0

    for room in Room.objects.all():
        if new_booking.room.room_type.strip().casefold() == room.room_type.strip().casefold():
            night_price = room.price.price

    if night_price != 0:
        diff = abs((new_booking.check_out - new_booking.check_in).days)
        price = night_price * diff
        return price, diff
    else:
        return price, diff


def send_email(new_booking, price, diff):
    subject = "Booking"
    message = """
                        <div>
                            <h1>Dear {name} {surname}!</h1>
                            <p>The Hotel23 family appreciates your accommodation choice!
                            We are happy to accompany you at the second step of the booking process!
                            Stay strong!</p>
                            <p>We are waiting for you at the address: Zanzibara 20, Warsaw</p>
                            <table>
                                <tr>
                                    <td style="padding: 10px; border: #e9e9e9 1px solid; text-align: center;">Client:</td>
                                    <td style="padding: 10px; border: #e9e9e9 1px solid; text-align: center;">{client_name}</td>
                                </tr>
                                <tr>
                                    <td style="padding: 10px; border: #e9e9e9 1px solid; text-align: center;">Arrival date:</td>
                                    <td style="padding: 10px; border: #e9e9e9 1px solid; text-align: center;">{check_in}</td>
                                </tr>
                                <tr>
                                    <td style="padding: 10px; border: #e9e9e9 1px solid; text-align: center;">Departure date:</td>
                                    <td style="padding: 10px; border: #e9e9e9 1px solid; text-align: center;">{check_out}</td>
                                </tr>
                                <tr>
                                    <td style="padding: 10px; border: #e9e9e9 1px solid; text-align: center;">Nights:</td>
                                    <td style="padding: 10px; border: #e9e9e9 1px solid; text-align: center;">{nights}</td>
                                </tr>
                                <tr>
                                    <td style="padding: 10px; border: #e9e9e9 1px solid; text-align: center;">Total price:</td>
                                    <td style="padding: 10px; border: #e9e9e9 1px solid; text-align: center;">{total_price}</td>
                                </tr>
                            </table>
                            <p>To proceed with your booking, please send {total_price} PLN (zloty) to the specified bank account.
                            In the payment description, put this number: {booking_id}</p>
                            <p>Bank account number: PL551320153745505330000001</p>
                        </div>
                    """.format(
        name=new_booking.name,
        surname=new_booking.surname,
        client_name=new_booking.name + " " + new_booking.surname,
        check_in=str(new_booking.check_in),
        check_out=str(new_booking.check_out),
        total_price=str(price),
        booking_id=str(new_booking.id),
        nights=str(diff)
    )

    from_email = settings.DEFAULT_FROM_EMAIL
    to = new_booking.email
    text_content = strip_tags(message)
    email_message = EmailMultiAlternatives(subject, text_content, from_email, [to])
    current_dir = os.path.dirname(os.path.abspath(__file__))
    image_path = os.path.join(current_dir, 'static/assets/img/Hotel23.png')
    with open(image_path, 'rb') as f:
        email_message.attach('Hotel23.png', f.read(), 'image/png')
    email_message.attach_alternative(message, "text/html")
    try:
        email_message.send()
        return True
    except BadHeaderError:
        return False


def book(request):
    if request.method == 'POST':
        form = BookingForm(request.POST)
        if form.is_valid():
            new_booking = form.save()
            is_available = check_availability(new_booking)
            if is_available:
                price, diff = calculate_price(new_booking)
                status = send_email(new_booking, price, diff)
                data = {'booking': new_booking, 'total_price': price}
                if status:
                    HttpResponse("Success!")
                    return render(request, 'success.html', data)
                else:
                    return HttpResponse("Invalid header found.")
            else:
                return HttpResponse("Sorry, there is no available room of the type you chose in this dates")
        else:
            HttpResponse('Form was NOT submitted!!! Check data')
            return render(request, 'fail.html')
    else:
        return render(request, "index.html", {'form': BookingForm()})


def success(request):
    return render(request, 'success.html')


def fail(request):
    return render(request, 'fail.html')


def comments(request):
    form = CommentForm()
    all_comments = Comment.objects.order_by('-created_at', 'rating')
    return render(request, 'comments.html', {'form': form, "comments": all_comments})


def do_comment(request):
    form = CommentForm(request.POST)
    all_comments = Comment.objects.order_by('-created_at', 'rating')
    if request.method == "POST":
        if form.is_valid():
            new_comment = form.save()
            HttpResponse("Comment was submitted")
            return render(request, 'comments.html', {'form': CommentForm(), "comments": all_comments})
        else:
            HttpResponse("Oooops... Your comment was not submitted")
            return render(request, 'comments.html', {'form': CommentForm(), "comments": all_comments})
    else:
        return render(request, 'comments.html', {'form': form, "comments": all_comments})
