/*!
* Start Bootstrap - Agency v7.0.12 (https://startbootstrap.com/theme/agency)
* Copyright 2013-2023 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-agency/blob/master/LICENSE)
*/
//
// Scripts
// 

window.addEventListener('DOMContentLoaded', event => {

    // Navbar shrink function
    var navbarShrink = function () {
        const navbarCollapsible = document.body.querySelector('#mainNav');
        if (!navbarCollapsible) {
            return;
        }
        if (window.scrollY === 0) {
            navbarCollapsible.classList.remove('navbar-shrink')
        } else {
            navbarCollapsible.classList.add('navbar-shrink')
        }

    };

    // Shrink the navbar 
    navbarShrink();

    // Shrink the navbar when page is scrolled
    document.addEventListener('scroll', navbarShrink);

    //  Activate Bootstrap scrollspy on the main nav element
    const mainNav = document.body.querySelector('#mainNav');
    if (mainNav) {
        new bootstrap.ScrollSpy(document.body, {
            target: '#mainNav',
            rootMargin: '0px 0px -40%',
        });
    };

    // Collapse responsive navbar when toggler is visible
    const navbarToggler = document.body.querySelector('.navbar-toggler');
    const responsiveNavItems = [].slice.call(
        document.querySelectorAll('#navbarResponsive .nav-link')
    );
    responsiveNavItems.map(function (responsiveNavItem) {
        responsiveNavItem.addEventListener('click', () => {
            if (window.getComputedStyle(navbarToggler).display !== 'none') {
                navbarToggler.click();
            }
        });
    });
});

// For Services
function enlarge(element) {
  element.classList.add('enlarged');
  element.classList.remove('smaller');
}

function resetSize(element) {
  element.classList.remove('enlarged');
  element.classList.add('smaller');
}

//Copy contacts to clipboard
// const telephone = document.querySelector("#telephone")
// const mail = document.querySelector('#mail')
// const messageCopy = "Copy text"
// const messageSuccess = 'Copied to clipboard';
//
// mail.append('<span class="message"></span>');
// telephone.append('<span class="message"></span>');
//
// telephone.addEventListener('click', () => {
// 	let text = telephone.textContent;
// 	navigator.clipboard.writeText(`${text} `);
//     $('.message').empty().append(messageSuccess);
//     setTimeout(function() {
//       $('.message').empty().append(messageCopy);}, 2000);
// });
// mail.addEventListener('click', () => {
// 	let text = mail.textContent;
// 	navigator.clipboard.writeText(`${text} `);
//     $('.message').empty().append(messageSuccess);
//     setTimeout(function() {
//       $('.message').empty().append(messageCopy);}, 2000);
// });


$(document).ready(function() {
  const telephone = $('#telephone');
  const mail = $('#mail');
  const messageCopy = 'Copy text';
  const messageSuccess = 'Copied to clipboard';

  telephone.append('<span class="message"></span>');
  mail.append('<span class="message"></span>');

  $('.message').text(messageCopy);

  telephone.on('click', function() {
    let text = $(this).text().trim();
    navigator.clipboard.writeText(text).then(() => {
      const messageElement = $(this).find('.message');
      messageElement.text(messageSuccess).show();
      setTimeout(function() {
        messageElement.text(messageCopy).hide();
      }, 2000);
    });
  });

  mail.on('click', function() {
    let text = $(this).text().trim();
    navigator.clipboard.writeText(text).then(() => {
      const messageElement = $(this).find('.message');
      messageElement.text(messageSuccess).show();
      setTimeout(function() {
        messageElement.text(messageCopy).hide();
      }, 2000);
    });
  });

  $('.copy-me').hover(
    function() {
      $(this).find('.message').show();
    },
    function() {
      $(this).find('.message').hide();
    }
  );
});

