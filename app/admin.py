from django.contrib import admin
from .models import Booking, Price, Comment, Room

admin.site.register(Price)


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    list_display = ("room_type", "price", )
    ordering = ("price", )
    search_fields = ("name", "price", )


@admin.register(Booking)
class BookingAdmin(admin.ModelAdmin):
    list_display = ("name", "surname", "email", "phone", "payment_status",)
    ordering = ("-check_in", "created_at",)
    search_fields = ("name", "surname", "phone", "email",)


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ("name", "surname", "rating",)
    search_fields = ("name", "rating",)