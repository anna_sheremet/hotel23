# Generated by Django 4.2 on 2023-07-12 21:53

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0009_price_alter_booking_check_in_alter_booking_check_out_and_more"),
    ]

    operations = [
        migrations.CreateModel(
            name="Comment",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.TextField(max_length=50)),
                ("surname", models.TextField(max_length=100)),
                ("stars", models.IntegerField(null=True)),
                ("comment", models.TextField(max_length=1000)),
                (
                    "created_at",
                    models.DateTimeField(
                        default=datetime.datetime(2023, 7, 12, 21, 53, 18, 338441)
                    ),
                ),
            ],
        ),
        migrations.AlterField(
            model_name="booking",
            name="check_in",
            field=models.DateField(default=datetime.date(2023, 7, 12)),
        ),
        migrations.AlterField(
            model_name="booking",
            name="check_out",
            field=models.DateField(default=datetime.date(2023, 7, 12)),
        ),
    ]
