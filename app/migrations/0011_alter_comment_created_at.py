# Generated by Django 4.2 on 2023-07-12 22:00

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0010_comment_alter_booking_check_in_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="comment",
            name="created_at",
            field=models.DateTimeField(
                default=datetime.datetime(2023, 7, 12, 22, 0, 16, 557772)
            ),
        ),
    ]
