# Generated by Django 4.2 on 2023-06-20 13:59

from django.db import migrations
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0006_alter_booking_check_in_alter_booking_check_out_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="booking",
            name="phone",
            field=phonenumber_field.modelfields.PhoneNumberField(
                max_length=128, region=None, unique=True
            ),
        ),
    ]
