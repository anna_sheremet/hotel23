from django.test import TestCase
from django.utils import timezone
from app.models import Booking, Comment, Price, Room


class BookingTest(TestCase):
    fixtures = ['initial_data.json']

    def create_booking(self, check_in=None, check_out=None, guests_number='1', name='Jack', surname='Doe',
                       phone='+1111111111', email='jack@example.com'):
        if check_in is None:
            check_in = timezone.now()
        if check_out is None:
            check_out = timezone.now() + timezone.timedelta(days=1)

        room = Room.objects.get(room_type='Twin')
        return Booking.objects.create(check_in=check_in, check_out=check_out, room=room, guests_number=guests_number,
                                      name=name, surname=surname, phone=phone, email=email)

    def test_booking_text_representation(self):
        b = self.create_booking()
        self.assertTrue(isinstance(b, Booking))
        self.assertEqual(b.__str__(), b.surname)


class CommentTest(TestCase):
    fixtures = ['initial_data.json']

    def create_comment(self, name="Jane", surname="Doe", rating="5", comment="Great", created_at=None):
        if created_at is None:
            created_at = timezone.now()
        return Comment.objects.create(name=name, surname=surname, rating=rating, comment=comment, created_at=created_at)

    def test_comment_text_representation(self):
        c = self.create_comment()
        self.assertTrue(isinstance(c, Comment))
        self.assertEqual(c.__str__(), c.surname)


class PriceTest(TestCase):
    fixtures = ['initial_data.json']

    def create_price(self, price=None):
        return Price.objects.create(price=price)

    def test_price_text_representation(self):
        p = self.create_price(price="100")
        self.assertTrue(isinstance(p, Price))
        self.assertEqual(p.__str__(), p.price)

    def test_fail_price_text_representation(self):
        p = self.create_price(price=100)
        self.assertTrue(isinstance(p, Price))
        self.assertNotEqual(p.__str__(), p.price)


class RoomTest(TestCase):
    fixtures = ['initial_data.json']

    def create_room(self, room_type="Double", price=None, description="spacious room"):
        if price is None:
            price = Price.objects.get(price=350)
        return Room.objects.create(room_type=room_type, price=price, description=description)

    def test_room_text_representation(self):
        r = self.create_room()
        self.assertTrue(isinstance(r, Room))
        self.assertEqual(r.__str__(), r.room_type)
